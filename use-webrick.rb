#!/usr/local/bin/ruby
# -*-coding: utf-8 -*-

require 'webrick'

class MyServlet < WEBrick::HTTPServlet::AbstractServlet
  def do_POST(req,res)
    res['content-type'] = 'text/plain'
    res.body << "You posted the following content: \n"
    res.body << req.body
  end
end
  
server = WEBrick::HTTPServer.new({
:DocumentRoot => './', 
:BindAddress => '127.0.0.1', 
:Port => 4000,
:CGIInterpreter => `which ruby`
})

server.mount('/application.rb',MyServlet)
server.mount("/books",WEBrick::HTTPServlet::CGIHandler,'application.rb')

trap("INT"){ server.shutdown }
server.start
