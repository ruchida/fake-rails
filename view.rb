#!/usr/local/bin/ruby
# -*- coding: utf-8 -*-

#モデル

require 'erb'

class View

  def self.index(books)
    erb = File.open('./index.html.erb') {|file| ERB.new(file.read)}
    puts
    puts erb.result(binding)
  end
 
  def self.show(book)
    erb = File.open('./show.html.erb') {|file| ERB.new(file.read)}
    puts
    puts erb.result(binding)
  end

  def self.new(book)
    erb = File.open('./new.html.erb') {|file| ERB.new(file.read)}
    puts
    puts erb.result(binding)
  end

  def self.edit(book)
    erb = File.open('./edit.html.erb') {|file| ERB.new(file.read)}
    puts
    puts erb.result(binding)
  end

  def self.create_entry_form(post_to,action,button_value,book)
    erb = File.open('./entry_form.html.erb') {|file| ERB.new(file.read)}
    erb.def_method(View,'self.create_form(post_to,action,button_value,book)','entry_form.html.erb')
    puts
    puts View.create_form(post_to,action,button_value,book)
  end

  protected

  def self.check_button_default(cd)
    if cd=='true'
      return "checked='checked'"
    else
      return ''
    end
  end

  def self.divide_date_into_ymd(date)
    year_month_date = date.split("-")
    return year_month_date
  end
end
