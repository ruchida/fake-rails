#!/usr/local/bin/ruby
# -*- coding: utf-8 -*-

require './book'
require 'erb'

class BookController

  def render(page,param)
    erb = File.open("./#{page}.html.erb") {|file| ERB.new(file.read)}
    puts
    puts erb.result(param)
  end

  def redirect_to(destination)
    puts "Location:#{destination}\n\n"
  end

  def format_date_in_param(params)
    format_date(params[:year],params[:month],params[:day])
  end

  def format_date(year, month, day)
    "#{year}-#{month}-#{day}"  
  end

  def index
    books = Book.all
    render("index",binding)
  end

  def show(isbn)
    book = Book.find(isbn)
    render("show",binding)   
  end

  def new
    now = Time.now
    book = Book.new({published: format_date(now.year,now.month,now.day)})
    render("new",binding)
  end

  def create(params)
    params[:published] = format_date_in_param(params)
    book = Book.new(params)
    book.create
    redirect_to("/books/#{book.isbn}")      
  end

  def edit(isbn)
    book = Book.find(isbn)
    render("edit",binding)  
  end

  def update(params)
    params[:published] = format_date_in_param(params)
    book = Book.new(params)
    book.update(params[:old_isbn])
    redirect_to("/books/#{book.isbn}")
  end

  def destroy(isbn)
    book = Book.find(isbn)
    book.destroy
    redirect_to("/books")
  end
end
