#!/usr/local/bin/ruby
# -*- coding: utf-8 -*-

require 'uri'

class Param
  def self.get_from_post()
    query = $stdin.read ENV['CONTENT_LENGTH'].to_i
    query = URI.decode(query)

    query = query.split("&")
    
    params = Array.new
    
    query.each do |part|
      params << part.split("=")
    end

    param = Hash.new

    params.each do |part|
      param[part[0].to_sym] = part[1]
    end
    return param
  end

  def self.get_path_info()
    if ENV['PATH_INFO'] == ''
      path_info = ''
    else
      path_info = ENV['PATH_INFO'].split("/")
    end
    return path_info
  end
end
