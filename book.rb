#!/usr/local/bin/ruby
# -*- coding: utf-8 -*-

require 'mysql2'
require 'uri'
require './location'

class Book
  attr_reader :isbn,:title,:price,:publish,:published,:cd

  def initialize(params = {})
    @isbn = params[:isbn]
    @title = params[:title]
    @price = params[:price]
    @publish = params[:publish]
    @published = params[:published]
    @cd = params[:cd]
  end
  
  def self.find(isbn)
    client = Book.access_to_table
    data = client.query("SELECT * FROM books_table WHERE isbn = '#{isbn}'")
    books = Array.new
    data.each do |row|
      row = Book.hash_to_symbol(row)
      book = Book.new(row)
      books << book
    end
    return books[0]
  end

  def self.all
    client = Book.access_to_table
    books_list = client.query("SELECT * FROM books_table") 

    books = Array.new
    books_list.each do |row|
      row = Book.hash_to_symbol(row)
      book = Book.new(row)
      books << book
    end
    return books
  end 

  def create
    client = Book.access_to_table
    client.query("INSERT INTO books_table VALUES(
      '#{@isbn}',
      '#{@title}',
      '#{@price}',
      '#{@publish}',
      '#{@published}',
      '#{@cd}'
    )")

  end

  def update(old_isbn)
    client = Book.access_to_table
    client.query("UPDATE books_table SET 
      isbn='#{@isbn}',
      title='#{@title}',
      price='#{@price}',
      publish='#{@publish}',
      published='#{@published}',
      cd='#{@cd}'
    WHERE isbn='#{old_isbn}'")

  end

  def destroy
    client = Book.access_to_table
    client.query("DELETE FROM books_table WHERE isbn = '#{@isbn}'")
  end

  def self.combine_ymd_into_date(year,month,day)
    if year != nil && month != nil && day != nil
      date = "#{year+'-'+month+'-'+day}"
      return date
    end
  end

#protected

  def self.access_to_table
    client =  Mysql2::Client.new(:host => "localhost", :username => "uryo", :password => "uryo_uses_mysql_912",:database => "books_db")
    client.query("SET NAMES 'utf8'")
    return client
  end
  def self.hash_to_symbol(params)
    hash = Hash.new
    params.each do |param|
      hash[param[0].to_sym] = param[1]
    end
    return hash
  end
end
