fake-railsでは，Railsの動作を模した処理をRubyで行います．具体的には，本の一覧に対する要素の追加や削除などの処理を行います．

○要インストール
fake-railsを動かすためには，以下の2つをインストールする必要があります．
・mysql2
・MySQL

mysql2は　gem install mysql2 コマンドでインストールできます．

MySQLは
http://www-jp.mysql.com/downloads/mysql/
からダウンロード，インストールしてください．

○起動手順

(1)
setup-books-table.rb を実行すると，MySQLのデータベース上にbooks_tableが作成され，テストデータが登録されます．
実行の前に6行目 :database => 'xxxx' の xxxx を変更し，参照先を任意のデータベースに設定してください． 

(2)
useWEBrick.rb を実行すると，ローカルサーバーが起動します．
URL localhost:4000/books がテストデータの一覧表示ページです．

※一覧表示ページはbooks.rbで作成しています．

○進捗状況
2013.4.5 11:38現在
(1)実装済み
・MySQL上のDBにテストデータを登録する
・テストデータをDBから取得する
・取得したデータを一覧表示する

(2)実装中
・一覧から本を削除する
・一覧に新たな本を追加する
