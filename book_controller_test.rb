#!/usr/local/bin/ruby
# -*- coding: utf-8 -*-

require 'minitest/unit'

MiniTest::Unit.autorun

class BookControllerTest < MiniTest::Unit::TestCase

  def setup
    @env = "env REQUEST_METHOD="
    @target = "book-controller.rb"
    @param = ""
    system("ruby setup-books-table.rb --delete books_db uryo uryo_uses_mysql_912")
  end

  def command(method,path_info)
    env = "#{@env}#{method} PATH_INFO=#{path_info} CONTENT_LENGTH=#{@param.length}"
    return "#{env} ruby #{@target}"
  end

  def test_index
    IO.popen(command("GET",""),"r") { |io|
      assert_match(/<title>Listing Books/,io.read,'This page is index') 
    }
  end

  def test_profile    
    IO.popen(command("GET","978-4-7980-2812-5"),"r") { |io|
      assert_match(/<title>Book Profile/,io.read,'This page is show') 
    }
  end

  def test_new
    IO.popen(command("GET","new"),"r") { |io|
      assert_match(/<title>New Book/,io.read,'This page is new') 
    }
  end

  def test_edit
    IO.popen(command("GET","978-4-7980-2812-5/edit"),"r") { |io|
      assert_match(/<title>Editing Book/,io.read,'This page is edit')
    }
  end

  def test_create_unique_isbn
    @param = "isbn=978-4-7777-7777-7&title=test_title&price=1000&publish=test_publish&published=2013-4-12&cd=false"
    IO.popen("env REQUEST_METHOD=POST PATH_INFO= CONTENT_LENGTH=#{@param.length} ruby book-controller.rb","r+") { |io|
      io.puts(@param)
      io.close_write
      assert_match(/^Location:\/books\/978-4-7777-7777-7/,io.read,'Jump to new book profile after create') 
    }
  end  

  def test_create_not_unique_isbn
    @param = "isbn=978-4-7980-2812-5&title=test_title&price=1000&publish=test_publish&published=2013-4-12&cd=false"
    IO.popen("env REQUEST_METHOD=POST PATH_INFO= CONTENT_LENGTH=#{@param.length} ruby book-controller.rb","r+") { |io|
      io.puts(@param)
      io.close_write
      assert_match(/^Location:\/books\/978-4-7980-2812-5/,io.read,'Jump to new book profile after create') 
    }
  end 

  def test_update_unique_isbn
    @param = "old_isbn=978-4-7980-2812-5&isbn=978-4-7980-2812-5&title=test_title&price=1000&publish=test_publish&published=2013-4-12&cd=false"
    IO.popen("env REQUEST_METHOD=POST PATH_INFO=978-4-7980-2812-5 CONTENT_LENGTH=#{@param.length} ruby book-controller.rb","r+") { |io|
      io.puts(@param)
      io.close_write
      assert_match(/^Location:\/books\/978-4-7980-2812-5/,io.read,'Jump to profile after update book') 
    }
  end

  def test_update_not_unique_isbn
    @param = "old_isbn=978-4-7980-2812-5&isbn=978-4-7741-4466-5&title=test_title&price=1000&publish=test_publish&published=2013-4-12&cd=false"
    IO.popen("env REQUEST_METHOD=POST PATH_INFO=978-4-3333-4444-6 CONTENT_LENGTH=#{@param.length} ruby book-controller.rb","r+") { |io|
      io.puts(@param)
      io.close_write
      assert_match(/^Location:\/books\/978-4-7741-4466-5/,io.read,'Jump to profile after update book') 
    }
  end

  def test_destroy_exist_isbn
    IO.popen(command("POST","978-4-7980-2812-5"),"r") { |io|
      assert_match(/^Location:\/books/,io.read,'Destroy book whose isbn exits') 
    }
  end

  def test_destroy_not_exist_isbn
    IO.popen(command("POST","111-1-1111-1111-1") ,"r") { |io|
      assert_match(/^Location:\/books/,io.read,'Destroy book whose isbn does not exist') 
    }
  end
end
