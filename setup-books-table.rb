#!/usr/local/bin/ruby
 # -*- coding: utf-8 -*-
 
 require 'mysql2'
require 'optparse'
 
preproc = {}

<<<<<<< HEAD
opt = OptionParser.new

opt.on("--create-table") { |v| preproc[:create_tbl] = v }
opt.on("--delete") { |v| preproc[:delete] = v }
opt.parse!

unless ARGV.size == 3
  puts "USAGE: ruby #{$0} dbname username password"
  exit
end
=======
require 'mysql2'
require 'optparse'

preproc = {}

opt = OptionParser.new

opt.on("--create-table") { |v| preproc[:create_tbl] = v }
opt.on("--delete") { |v| preproc[:delete] = v }
opt.parse!

unless ARGV.size == 3
  puts "USAGE: ruby #{$0} dbname username password"
  exit
end

dbname, username, password = ARGV
client = Mysql2::Client.new(:host => "localhost", :username => username, :password => password, :database => dbname)

client.query("SET NAMES 'utf8'")

if preproc[:create_tbl]
  client.query(
    "CREATE TABLE books_table(
      isbn char(30), title char(100), price char(6), publish char(20), published date, cd char(5)
    )"
  )
end

if preproc[:delete]
  client.query(
    "DELETE FROM books_table"
  )  
end

books_testdata = [
  {
    isbn: "978-4-7980-2812-5", 
    title: "MySQL逆引き大全460の極意", 
    price: "2730", 
    publish: "秀和システム",
    published: "2010-12-01", 
    cd: "false"
  },
  {
    isbn: "978-4-7741-4466-5",
    title: "JavaScript本格入門",
    price: "3129",
    publish: "技術評論社",
    published: "2010-11-26",
    cd: "false"
  },
  {
    isbn: "978-4-82228-425-0",
    title: "プログラムを作ろう！Microsoft ASP.NET 4 入門",
    price: "1995",
    publish: "日経BP社",
    published: "2010-11-11",
    cd: "true"
  },
  {
    id: "4",
    isbn: "978-4-82229-423-6",
    title: "プログラムを作ろう！Microsoft Visual C++ 2010 入門",
    price: "1995",
    publish: "日経BP社",
    published: "2010-09-06",
    cd: "true"
  },
  {
    isbn: "978-4-7980-2695-4",
    title: "Windows Azure 実践クラウド・プログラミング",
    price: "3360",
    publish: "秀和システム",
    published: "2010-07-29",
    cd: "false"
  },
  {
    isbn: "978-4-8443-2879-7",
    title: "改訂3版 基礎PHP",
    price: "3360",
    publish: "インプレスジャパン",
    published: "2010-06-18",
    cd: "true"
  },
  {
    isbn: "978-4-7981-2205-2",
    title: "Visual Studio 2010スタートアップ・ガイド",
    price: "2079",
    publish: "翔泳社",
    published: "2010-06-09",
    cd: "false"
  },
  {
    isbn: "978-4-8443-2865-0",
    title: "PerlフレームワークCatalyst入門",
    price: "4410",
    publish: "インプレスジャパン",
    published: "2010-05-20",
    cd: "false"
  },
  {
    isbn: "978-4-7981-2151-2",
    title: "独習PHP 第2版",
    price: "3360",
    publish: "翔泳社",
    published: "2010-04-12",
    cd: "false"
  },
  {
    isbn: "978-4-7741-4223-4",
    title: "Apacheポケットリファレンス",
    price: "2604",
    publish: "技術評論社",
    published: "2010-04-10",
    cd: "false"
  }
]

books_testdata.each do |book_testdata|
>>>>>>> 48e2b5f62b2273f5b2d37109b29b023b3c1f5ded

dbname, username, password = ARGV
client = Mysql2::Client.new(:host => "localhost", :username => username, :password => password, :database => dbname)
 
 client.query("SET NAMES 'utf8'")
 
if preproc[:create_tbl]
  client.query(
    "CREATE TABLE books_table(
      isbn char(30), title char(100), price char(6), publish char(20), published date, cd char(5)
    )"
  )
end

if preproc[:delete]
  client.query(
    "DELETE FROM books_table"
  )  
end
 
 books_testdata = [
   {
     isbn: "978-4-7980-2812-5", 
     title: "MySQL逆引き大全460の極意", 
     price: "2730", 
     publish: "秀和システム",
     published: "2010-12-01", 
     cd: "false"
   },
   {
     isbn: "978-4-7741-4466-5",
     title: "JavaScript本格入門",
     price: "3129",
     publish: "技術評論社",
     published: "2010-11-26",
     cd: "false"
   },
   {
     isbn: "978-4-82228-425-0",
     title: "プログラムを作ろう！Microsoft ASP.NET 4 入門",
     price: "1995",
     publish: "日経BP社",
     published: "2010-11-11",
     cd: "true"
   },
   {
     id: "4",
     isbn: "978-4-82229-423-6",
     title: "プログラムを作ろう！Microsoft Visual C++ 2010 入門",
     price: "1995",
     publish: "日経BP社",
     published: "2010-09-06",
     cd: "true"
   },
   {
     isbn: "978-4-7980-2695-4",
     title: "Windows Azure 実践クラウド・プログラミング",
     price: "3360",
     publish: "秀和システム",
     published: "2010-07-29",
     cd: "false"
   },
   {
     isbn: "978-4-8443-2879-7",
     title: "改訂3版 基礎PHP",
     price: "3360",
     publish: "インプレスジャパン",
     published: "2010-06-18",
     cd: "true"
   },
   {
     isbn: "978-4-7981-2205-2",
     title: "Visual Studio 2010スタートアップ・ガイド",
     price: "2079",
     publish: "翔泳社",
     published: "2010-06-09",
     cd: "false"
   },
   {
     isbn: "978-4-8443-2865-0",
     title: "PerlフレームワークCatalyst入門",
     price: "4410",
     publish: "インプレスジャパン",
     published: "2010-05-20",
     cd: "false"
   },
   {
     isbn: "978-4-7981-2151-2",
     title: "独習PHP 第2版",
     price: "3360",
     publish: "翔泳社",
     published: "2010-04-12",
     cd: "false"
   },
   {
     isbn: "978-4-7741-4223-4",
     title: "Apacheポケットリファレンス",
     price: "2604",
     publish: "技術評論社",
     published: "2010-04-10",
     cd: "false"
   }
 ]
 
 books_testdata.each do |book_testdata|
 
   client.query ("
     INSERT INTO books_table VALUES (
       '#{book_testdata[:isbn]}',
       '#{book_testdata[:title]}',
       '#{book_testdata[:price]}',
       '#{book_testdata[:publish]}',
       '#{book_testdata[:published]}',
       '#{book_testdata[:cd]}'
     )
   ")
 end
