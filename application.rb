#!/usr/local/bin/ruby
# -*- coding: utf-8 -*-

require './param'
require './book-controller'

path_info = Param.get_path_info
params = Param.get_from_post
method = (params[:_method] || ENV['REQUEST_METHOD']).upcase

book_controller = BookController.new

case path_info[-1]

  when nil
    case method
      when 'GET'
        book_controller.index
      when 'POST'
        book_controller.create(params)
    end

  when /\d{3}-\d*-\d*-\d*-\d/ #isbnかどうかの判定
    case method
      when 'GET'
        isbn = path_info[-1]
        book_controller.show(isbn)
      when 'DELETE'
        isbn = path_info[-1]
        book_controller.destroy(isbn)
      when 'POST'
        book_controller.update(params)
    end

  when "new"
    book_controller.new
  when "edit"
    isbn = path_info[-2]
    book_controller.edit(isbn)
end
