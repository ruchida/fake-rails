#!/usr/local/bin/ruby
# -*- coding: utf-8 -8

#モデル

def setDefaultValues(isbn,title,price,publish,year,month,day,cd)
  default = {
    isbn: isbn,
    title: title,
    price: price,
    publish: publish,
    year: year,
    month: month,
    day: day,
    cd: cd
  }
  return default
end
