# encoding: utf-8
require 'webrick'
srv = WEBrick::HTTPServer.new({:DocumentRoot => '/users/uchida/documents/fake-rails', :BindAddress => '127.0.0.1', :Port => 20080})

#data
ISBN = ["978-4-7980-2812-5","978-4-7741-4466-5","978-4-82228-425-0","978-4-82229-423-6","978-4-7980-2695-4","978-4-8443-2879-7","978-4-7981-2205-2","978-4-8443-2865-0","978-4-7981-2151-2","978-4-7741-4223-4"]
Title = ["MySQL逆引き大全460の極意","JavaScript本格入門","プログラムを作ろう！Microsoft ASP.NET 4 入門","プログラムを作ろう！Microsoft Visual C++ 2010 入門","Windows Azure 実践クラウド・プログラミング","改訂3版 基礎PHP","Visual Studio 2010スタートアップ・ガイド","PerlフレームワークCatalyst完全入門","独習PHP 第2版","Apacheポケットリファレンス"]
Price = ["2730","3129","1995","1995","3360","3360","2079","4410","3360","2604"]
Publish = ["秀和システム","技術評論社","日経BP社","日経BP社","秀和システム","インプレスジャパン","翔泳社","インプレスジャパン","翔泳社","技術評論社"]
Published = ["2010-12-01","2010-11-26","2010-11-11","2010-09-06","2010-07-29","2010-06-18","2010-06-09","2010-05-20","2010-04-12","2010-04-10"]
CD = ["false","false","true","true","false","true","false","false","false","false"]


srv.mount_proc("/")do |req,res|
  res.content_type="text/html"
  res.body="
<!DOCTYPE HTML>
<meta http-equiv=Content-Type content=text/html; charset=UTF-8>
<html>
  <head>
    <title>Railbook</title>
    <base href=http://localhost:20080/books/>
  </head>

  <body>
  <p><font size=5>Listing books</font></p>

<table>
  <tr>
    <th>Isbn</th>
    <th>Title</th>
    <th>Price</th>
    <th>Publish</th>
    <th>Published</th>
    <th>Cd</th>
    <th></th>
    <th></th>
    <th></th>
  </tr>
  <tr>
    <td> #{ISBN[0]}</td>
    <td>#{Title[0]}</td>
    <td>#{Price[0]}</td>
    <td>#{Publish[0]}</td>
    <td>#{Published[0]}</td>
    <td>#{CD[0]}</td>
    <td><a href=/books/1>Show</a></td>
    <td><a href=/books/1/edit>Edit</a></td>
    <td>Destroy</td>
  </tr>
  <tr>
    <td>#{ISBN[1]}</td>
    <td>#{Title[1]}</td>
    <td>#{Price[1]}</td>
    <td>#{Publish[1]}</td>
    <td>#{Published[1]}</td>
    <td>#{CD[1]}</td>
    <td><a href=/books/2>Show</a></td>
    <td><a href=/books/2/edit>Edit</a></td>
    <td>Destroy</td>
  </tr>
  <tr>
    <td>#{ISBN[2]}</td>
    <td>#{Title[2]}</td>
    <td>#{Price[2]}</td>
    <td>#{Publish[2]}</td>
    <td>#{Published[2]}</td>
    <td>#{CD[2]}</td>
    <td><a href=/books/3>Show</td>
    <td><a href=/books/3/edit>Edit</td>
    <td>Destroy</td>
  </tr>
  <tr>
    <td>#{ISBN[3]}</td>
    <td>#{Title[3]}</td>
    <td>#{Price[3]}</td>
    <td>#{Publish[3]}</td>
    <td>#{Published[3]}</td>
    <td>#{CD[3]}</td>
    <td><a href=/books/4>Show</td>
    <td><a href=/books/4/edit>Edit</td>
    <td>Destroy</td>
  </tr>
  <tr>
    <td>#{ISBN[4]}</td>
    <td>#{Title[4]}</td>
    <td>#{Price[4]}</td>
    <td>#{Publish[4]}</td>
    <td>#{Published[4]}</td>
    <td>#{CD[4]}</td>
    <td><a href=/books/5>Show</td>
    <td><a href=/books/5/edit>Edit</td>
    <td>Destroy</td>
  </tr>
  <tr>
    <td>#{ISBN[5]}</td>
    <td>#{Title[5]}</td>
    <td>#{Price[5]}</td>
    <td>#{Publish[5]}</td>
    <td>#{Published[5]}</td>
    <td>#{CD[5]}</td>
    <td><a href=/books/6>Show</td>
    <td><a href=/books/6/edit>Edit</td>
    <td>Destroy</td>
  </tr>
  <tr>
    <td>#{ISBN[6]}</td>
    <td>#{Title[6]}</td>
    <td>#{Price[6]}</td>
    <td>#{Publish[6]}</td>
    <td>#{Published[6]}</td>
    <td>#{CD[6]}</td>
    <td><a href=/books/7>Show</td>
    <td><a href=/books/7/edit>Edit</td>
    <td>Destroy</td>
  </tr>
  <tr>
    <td>#{ISBN[7]}</td>
    <td>#{Title[7]}</td>
    <td>#{Price[7]}</td>
    <td>#{Publish[7]}</td>
    <td>#{Published[7]}</td>
    <td>#{CD[7]}</td>
    <td><a href=/books/8>Show</td>
    <td><a href=/books/8/edit>Edit</td>
    <td>Destroy</td>
  </tr>
  <tr>
    <td>#{ISBN[8]}</td>
    <td>#{Title[8]}</td>
    <td>#{Price[8]}</td>
    <td>#{Publish[8]}</td>
    <td>#{Published[8]}</td>
    <td>#{CD[8]}</td>
    <td><a href=/books/9>Show</td>
    <td><a href=/books/9/edit>Edit</td>
    <td>Destroy</td>
  </tr>
  <tr>
    <td>#{ISBN[9]}</td>
    <td>#{Title[9]}</td>
    <td>#{Price[9]}</td>
    <td>#{Publish[9]}</td>
    <td>#{Published[9]}</td>
    <td>#{CD[9]}</td>
    <td><a href=/books/10>Show</a></td>
    <td><a href=/books/10/edit>Edit</a></td>
    <td>Destroy</td>
  </tr>

</table>

<br />

New Book
  </body>
</html>"
end
trap("INT"){ srv.shutdown }
srv.start
