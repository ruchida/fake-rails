# -*- coding: utf-8 -*-
def checkBlank(input)
  if input==''
    puts '入力は必須です．'
  end
end

def checkIsbn(isbn_input)
  checkBlank(isbn_input)

  pattern1 = /^978-\d-\d\d\d\d\d-\d\d\d-\d$/
  pattern2 = /^978-\d-\d\d\d\d-\d\d\d\d-\d$/

# ~=:パターンマッチ演算子

  if isbn_input != pattern1 && isbn_input != pattern2
    puts 'isbnを入力してください．'
  end  
end

def checkTitle(title_input)
  checkBlank(title_input)
end

def checkPrice(price_input)
  checkBlank(price_input)
  if price_input != /^[0-9]+$/
    puts '金額は数値を入力してください．'
  end
end

def checkPublish(publish_input)
  checkBlank(publish_input)
end

checkIsbn('not_num!')
