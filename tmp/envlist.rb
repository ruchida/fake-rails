#!/usr/local/bin/ruby
# -*- coding: utf-8 -*-

print "Content-type: text/html\n\n"
print <<END
    <html>
    <head>
    <title>環境変数一覧</title>
    </head>
    <body>
    <table align="center" border="1" bordercolor="#000099" cellspacing="0">
    <caption>環境変数一覧</caption>
END

ENV.each { |k,v|
  print "<tr><td>#{k}</td><td>#{v}</td></tr>\n"
}

print <<END
    </table>
    </body>
    </html>
END

exit
