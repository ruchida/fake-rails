#!/usr/local/bin/ruby
# -*- coding: utf-8 -*-

#ビュー

def checkButtonDefault(cd)
  if cd=='true'
    return 'checked'
  else
    return ''
  end
end

def createEntryForm(cgi,button_value,isbn_default,title_default,price_default,publish_default,year_default,month_default,day_default,cd_default,action,send_param_name,send_param_value)

print"
<!DOCTYPE HTML>
<html>
  <head>
    <meta http-equiv='Content-Type' content='text/html;charset=UTF-8'>
  </head>
  <body>
<h1>"
if action=="new"
  print "New Book"
else
  print "Edit Book"
end
print "
</h1>

<form action='#{cgi}' method='post' accept-charset='UTF-8'>

  <div class='field'>
    <label for='book_isbn'>Isbn</label><br />
    <input name='isbn' id='book_isbn' size='30' type='text' value='#{isbn_default}' />
  </div>
  <div class='field'>
    <label for='book_title'>Title</label><br />
    <input name='title' id='book_title' size='30' type='text' value='#{title_default}' />
  </div>
  <div class='field'>
    <label for='book_price'>Price</label><br />
    <input name='price' id='book_price' type='number' value='#{price_default}' />
  </div>
  <div class='field'>
    <label for='book_publish'>Publish</label><br />
    <input name='publish' id='book_publish' size='30' type='text' value='#{publish_default}' /> 
  </div>
  <div class='field'>
    <label for='book_published'>Published</label><br />
    <select id='book_published_1i' name='year' value='#{year_default}'>"

  (2008..2018).each do |year|
      year = year.to_s
    if year==year_default then
      print "<option selected='selected' value='#{year}'>#{year}</option>"

    else
      print "<option value='#{year}'>#{year}</option>"
    end
  end

print "
  </select>
"
  months = {
   "1"=> "January",
   "2"=> "February",
   "3"=> "March",
   "4"=> "April",
   "5"=> "May",
   "6"=> "June",
   "7"=> "July",
   "8"=> "August",
   "9"=> "September",
   "10"=> "October",
   "11"=> "November",
   "12"=> "December"
  }

  print "
    <select id='book_published_2i' name='month'>
  "
  (1..12).each do |month|
    month = month.to_s

    if "0"+month==month_default || month==month_default
      print "
        <option selected='selected' value='#{month}'>#{months[month]}</option>
    "
    else
      print "
        <option value='#{month}'>#{months[month]}</option>
      "
    end
  end
  print "
    </select>
    <select id='book_published_3i' name='day'>
  "

  (1..31).each do |day|
    if day < 10
      day = "0" + day.to_s
    else
      day = day.to_s
    end

    if day==day_default then
      print "
        <option selected='selected' value='#{day}'>#{day}</option>
      "
    else
      print "
        <option value='#{day}'>#{day}</option>
      "
    end
  end

  print "
    </select>

  </div>
  <div class='field'>
    <label for='book_cd'>CD</label><br />
    <input name='cd' type='hidden' value='false' />
    <input id='book_cd' name='cd' type='checkbox' value='true',checked='#{checkButtonDefault(cd_default)}' />
  </div>
    <input name='submit' type='submit' value='#{button_value}' />
    <input type='hidden' name='#{send_param_name}' value='#{send_param_value}' />
</form>

<a href='/books'>Back</a>

</body>
</html>
"
end
