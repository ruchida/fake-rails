#!/usr/local/bin/ruby
# -*- coding: utf-8 -*-

require './book'

books = Book::get_list('books_table')

print "
<!DOCTYPE HTML>
<html>
  <head>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
    <TITLE>Railbook</TITLE>
    <script type='text/JavaScript'>
    <!--
    function destroy_check() {
      if(!window.confirm('Are you sure?')) {
        return false;
      } else {
          return true;
      }
    }
    //-->
  </script>
  </head>

  <body>
  <h1>Listing books</h1>

  <table id='4000'>
    <tr>
      <th>ISBN</th>
      <th>Title</th>
      <th>Price</th>
      <th>Publish</th>
      <th>Published</th>
      <th>CD</th>
       <th></th>
      <th></th>
     <th></th>
    </tr>"

books.each do |book|
  print "
      <tr>
      <td> #{book[:isbn]}</td>
      <td> #{book[:title]}</td>
      <td> #{book[:price]}</td>
      <td> #{book[:publish]}</td>
      <td> #{book[:published]}</td>
      <td> #{book[:cd]}</td>
      <td><form action='/books/show/#{book[:isbn]}' method='post' accept-charset='utf-8'>
            <input type='submit' name='show' value='show' />
            <input type='hidden' name='isbn' value='#{book[:isbn]}' />
            <input type='hidden' name='command' value='show'>
        </form>
      </td>
      
      <td><form action='/books/edit/#{book[:isbn]}' method='post' accept-charset='utf-8'>
            <input type='submit' name='edit' value='edit' />
            <input type='hidden' name='isbn' value='#{book[:isbn]}' />
            <input type='hidden' name='title' value='#{book[:title]}' />
            <input type='hidden' name='price' value='#{book[:price]}' />
            <input type='hidden' name='publish' value='#{book[:publish]}' />
            <input type='hidden' name='published' value='#{book[:published]}' />
            <input type='hidden' name='cd' value='#{book[:cd]}' />
            <input type='hidden' name='command' value='edit' />
          </form>
      </td>
      <td><form action='/books/destroy' method='post' accept-charset='UTF-8' rel='nofollow'>
            <input type='submit' name='destroy' value='destroy' onClick='return destroy_check()' />
            <input type='hidden' name='destroy_isbn' value='#{book[:isbn]}' />
            <input type='hidden' name='command' value='destroy' />
          </form>
      </td>
     </tr>"
end
print "
</table>

<br />

<form action='/books/new' method='post'>
  <input type='submit' name='new' value='New Book' />
  <input type='hidden' name='new_id' value='#{books[-1][:isbn]}'>
  <input type='hidden' name='command' value='new'>
  </body>
</html>"
