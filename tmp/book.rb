#!/usr/local/bin/ruby
# -*- coding: utf-8 -*-

module Book
  def self.getParam()
    query = $stdin.read ENV['CONTENT_LENGTH'].to_i
    query = URI.decode(query)
    received_param = query.split("&").split("=")

    param = Hash.new
    received_param.each do |part|
      param[part[0]] = part[1]
    end
    return param
  end

  def self.getParamByIsbn(table_name,isbn)
    client = self.accessToTable()
    book = client.query("SELECT * FROM #{table_name} WHERE isbn = #{isbn}")
    param = {
      isbn: book["isbn"],
      title: book["title"],
      price: book["price"],
      publish: book["publish"],
      published: book["published"],
      cd: book["cd"]
    }
    return param
  end

  def self.getList(table_name)
    client = self.accessToTable()
    books_list =  client.query("SELECT * FROM #{table_name}") 
    books = Array.new

    books_list.each do |row|
      books << {
        isbn: row["isbn"],
        title: row["title"],
        price: row["price"],
        publish: row["publish"],
        published: row["published"],
        cd: row["cd"]
      }
    end
    return books
  end 

  def self.showProfile(isbn,title,price,publish,published,cd)
    
    print "
<!DOCTYPE HTML>
<html>
    <head>
      <meta http-equiv='Content-Type' content='text/html;charset=utf-8'>
      <title></title>
    </head>
    <body>
      <p>
        <b>Isbn:</b>
          #{isbn}
      </p>
      <p>
        <b>Title:</b>
          #{title}
      </p>
      <p>
        <b>Price:</b>
          #{price}
      </p>
      <p>
        <b>Publish:</b>
          #{publish}
      </p>
      <p>
        <b>Published:</b>
          #{published}
      </p>
      <p>
        <b>Cd:</b>
          #{cd}
      </p>

      <form action='/books/edit/#{isbn}' method='post'>
        <input type='submit' name='edit' value='Edit'>
        <input type='hidden' name='isbn' value='#{isbn}'>
        <input type='hidden' name='title' value='#{title}'>
        <input type='hidden' name='price' value='#{price}'>
        <input type='hidden' name='publish' value='#{publish}'>
        <input type='hidden' name='published' value='#{published}'>
        <input type='hidden' name='cd' value='#{cd}'>
      <a href='/books'>Back</a>
  </body>
</html>
"

  end

  def self.create(table_name,isbn,title,price,publish,published,cd)
    client = self.accessToTable()
    client.query("INSERT INTO #{table_name} VALUES('#{isbn}','#{title}','#{price}','#{publish}','#{published}','#{cd}')")
  end

  def self.editParam(table_name,old_isbn,new_isbn,title,price,publish,published,cd)
    client = self.accessToTable()
    client.query("UPDATE #{table_name} SET isbn='#{new_isbn}',title='#{title}',price='#{price}',publish='#{publish}',published='#{published}',cd='#{cd}' WHERE isbn = '#{old_isbn}'")
  end

  def self.destroy(table_name,isbn)
    client = self.accessToTable()
    client.query("DELETE FROM #{table_name} WHERE isbn = '#{isbn}'")
    locate("/books")
  end

  private

  def self.accessToTable()
    client =  Mysql2::Client.new(:host => "localhost", :username => "uryo", :password => "uryo_uses_mysql_912",:database => "books_db")
    client.query("SET NAMES 'utf8'")

    return client
  end

module_function :getParam
module_function :getParamByIsbn
module_function :getList
module_function :showProfile
module_function :create
module_function :editParam
module_function :destroy
end
