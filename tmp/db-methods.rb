# -*- coding: utf-8 -*-
require 'mysql2'
require'./location'

#モデル

def accessToMySQL()
  client = Mysql2::Client.new(:host => "localhost", :username => "uryo", :password => "uryo_uses_mysql_912",:database => "books_db")
  client.query("SET NAMES 'utf8'")
  return client
end

def setDBEncoding()
  client = accessToMySQL()
  client.query("SET NAMES 'utf8'")
end

def countRows()
  client = accessToMySQL()
  count = client.query("COUNT(*) FROM books_table")
end
  

def addBook(table_name,isbn,title,price,publish,published,cd)
  client = accessToMySQL()
  client.query("INSERT INTO #{table_name} VALUES('#{isbn}','#{title}','#{price}','#{publish}','#{published}','#{cd}')")
end

def destroyBook(table_name,isbn)
  client = accessToMySQL()
  client.query("DELETE FROM #{table_name} WHERE isbn = '#{isbn}'")
  locate("/books")
end

def getTable(table_name)
  client = accessToMySQL()
  table = client.query("SELECT * FROM #{table_name}")
  return table
end

def getOneRecord(table_name,isbn)
  client = accessToMySQL()
  book = client.query("SELECT * FROM #{table_name} WHERE isbn = '#{isbn}'")
  return book
end

def changeRecordParam(table_name,old_isbn,new_isbn,title,price,publish,published,cd)
  client = accessToMySQL()
  client.query("UPDATE #{table_name} SET isbn='#{new_isbn}',title='#{title}',price='#{price}',publish='#{publish}',published='#{published}',cd='#{cd}' WHERE isbn = '#{old_isbn}'")
end
