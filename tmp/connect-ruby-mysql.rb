# -*- coding: utf-8 -*-
require 'mysql2'
client = Mysql2::Client.new(:host=>"localhost", :username =>"uryo", :password=>"uryo_uses_mysql_912",:database=>"db1")

client.query('CREATE TABLE profile2(name char(10), age int, hobby char(10))')

#initialize
client.query("INSERT INTO profile2 VALUES( '#{'内田'}',#{24},'#{'トロンボーン'}')")
client.query("INSERT INTO profile2 VALUES('#{'小池'}',#{32},'#{'盆栽'}')")

puts "Before changed"
client.query("SELECT name,age,hobby FROM profile2 ").each do |col|
  p col
end

#add records to table

newperson_name = 'John'
newperson_age = 15
newperson_hobby = 'skiing'
client.query("INSERT INTO profile2 (name,age,hobby) VALUES('#{val1}',#{val2},'#{val3}')")

puts "After adding records"
client.query("SELECT name,age,hobby FROM profile2 ").each do |col|
  p col
end

#delete added records

client.query("DELETE FROM profile2 WHERE name = '#{val1}'")

puts "After deleting records"
client.query("SELECT name,age,hobby FROM profile2").each do |col|
  p col
end

#delete profile2 to repeat this program
client.query("DROP TABLE profile2")

client.close
