#!/usr/local/bin/ruby
# -*- coding: utf-8 -*-

require './view'
require './param'

params = Param::get

if params["command"] == "new"
  View::create_entry_form('/books/create','Create Book','','','','',"#{Time.now.year.to_s+'-'+Time.now.month.to_s+'-'+Time.now.day.to_s}",'','New','','',"create")

end

if params["command"] == "edit"
  View::create_entry_form("/books/update/#{params['isbn']}",'Edit Book',params["isbn"],params["title"],params["price"],params["publish"],params["published"],params["cd"],"Edit",'old_isbn',"#{params['isbn']}","update")

end
